<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use backend\models\PermissionForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => false,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'rateLimiter' => ['class' => 'yii\filters\RateLimiter'],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Nowy użytkownik został utworzony. Na podany adres email został wysłany mail z instrukcją do zalogowania. Pamiętaj, że użytkownik <b>nie będzie mógł się zalogować</b> na swoje konto dopóki nie utrzyma <b>odpowiednich uprawnień</b>!');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionPermissions($id) {
        $user = $this->findModel($id);
        $model = new PermissionForm();
        
        if($model->load(Yii::$app->request->post()) && $model->assign($user)) {
            Yii::$app->session->addFlash('success', 'Poprawnie zmieniono uprawnienia użytkownika.');
            return $this->redirect(['view', 'id' => $user->id]);
        } else {
            $roles = Yii::$app->authManager->getRolesByUser($user->id);
            foreach($roles as $role) {
                $model->roles[] = $role->name;
            }
        }
        
        return $this->render('permission', [
            'model' => $model,
            'user' => $user
        ]);
    }
    
    /* public function actionList($q = '', $page = 1) {
        Yii::$app->response->format = 'json';
        
        $query = User::find()
                ->select(['id', 'username AS text'])
                ->filterWhere(['like', 'username', $q])
                // ->indexBy('id')
                ->asArray()
                ->limit(Yii::$app->params['listQueryDefaultPageSize'])
                ->offset(($page - 1) * Yii::$app->params['listQueryDefaultPageSize'])
                ->all();
        
        return ['results' => $query];
    } */
}
