<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use common\models\SettingsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SettingsController implements the CRUD actions for Settings model.
 */
class SettingsController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'flush-cache' => ['post']
                ],
            ],
            'rateLimiter' => ['class' => 'yii\filters\RateLimiter'],
        ];
    }

    /**
     * Lists all Settings models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingsSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $rows = (new \yii\db\Query())
            ->select(['key', 'value'])
            ->from('settings')
            ->indexBy('key')
            ->all();
        
        $data = [];
        foreach(Settings::getValues('keys') as $key => $val) {
            if(!isset($val['group'])) {
                $val['group'] = $searchModel::defaultGroup;
            }
            if(!isset($data[$val['group']])) {
                $data[$val['group']] = [];
            }
            $data[$val['group']][$key] = array_merge(['key' => $key, 'value' => (isset($rows[$key]) ? $rows[$key]['value'] : (isset($val['defaultValue']) ? $val['defaultValue'] : null))], $val); 
            unset($rows[$key]);
        }
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'data' => $data,
            'unused' => $rows
        ]);
    }

    /**
     * Displays a single Settings model.
     * @param string $id
     * @return mixed
     */
    /*public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }*/

    /**
     * Creates a new Settings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->key]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Updates an existing Settings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    /*public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->key]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }*/

    /**
     * Deletes an existing Settings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionEdit($id) {
        if(($key = Settings::getValue('keys', $id)) === null) {
            throw new \yii\base\InvalidParamException('Nieprawidłowy klucz ustawień.');
        }
        
        try {
            $model = $this->findModel($id);
        } catch (NotFoundHttpException $ex) {
            $model = new Settings();
            $model->key = $id;
            if(isset($key['defaultValue'])) {
                $model->value = $key['defaultValue'];
            }
        }
        
        if(isset($key['events'])) {
            foreach($key['events'] as $name => $event) {
                $model->on($name, $event, $key);
            }
        }
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('edit', [
                'model' => $model,
                'key' => $key
            ]);
        }
    }

    public function actionFlushCache() {
        if(Yii::$app->cache->flush()) {
            Yii::$app->session->addFlash('success', 'Poprawnie wyczyszczono pamięć podręczną.');
        } else {
            Yii::$app->session->addFlash('error', 'Nie udało się wyczyszcić pamięci podręcznej.');
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Settings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Settings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
