<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use gandh1pl\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Url;

$this->title = 'Zaloguj';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box box box-danger">
    <div class="login-logo box-header with-border">
        <a href="<?= Url::toRoute(['site/login']) ?>"><b><?= Yii::$app->name ?></b></a>
    </div>
    <div class="login-box-body box-body">
        <p class="login-box-msg">Aby kontynuować, proszę się zalogować.</p>
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'fieldConfig' => function($model, $attribute) { 
                $icons = [
                    'username' => 'user',
                    'password' => 'lock',
                    'rememberMe' => 'plus',
                    'verifyCode' => 'ok'
                ];
                return [
                    'template' => '{input}{icon}'.(Yii::$app->request->isPost ? '{error}' : ''),
                    'inputOptions' => [
                        'placeholder' => $model->getAttributeLabel($attribute)
                    ],
                    'parts' => [
                        '{icon}' => '<span class="glyphicon glyphicon-'.$icons[$attribute].' form-control-feedback"></span>'
                    ],
                    'options' => ['class' => 'form-group has-feedback']
                ];
            },
        ]); ?>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= ($model->scenario === 'use-captcha' ? $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-6">{image}</div><div class="col-lg-6">{input}</div></div>',
                'imageOptions' => ['style' => 'cursor: pointer;']
            ]) : '') ?>


        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Zaloguj', ['class' => 'btn btn-primary btn-block btn-flat', 'icon' => 'lock']) ?>
            </div>
            <div class="col-xs-12 text-center">
                <?= Html::a('Przypomnij hasło', ['request-password-reset']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>