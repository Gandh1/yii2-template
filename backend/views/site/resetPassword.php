<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Url;
use gandh1pl\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Zmiana hasła';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box box box-primary">
    <div class="login-logo box-header with-border">
        <a href="<?= Url::toRoute(['site/login']) ?>"><b><?= Yii::$app->name ?></b></a>
    </div>
    <div class="login-box-body box-body">
        <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

        <p class="text-center">Prosimy podać Twoje nowe hasło:</p>

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Zapisz', ['class' => 'btn btn-primary', 'icon' => 'edit']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>