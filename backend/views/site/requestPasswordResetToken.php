<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use gandh1pl\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Odzyskiwanie hasła';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box box box-primary">
    <div class="login-logo box-header with-border">
        <a href="<?= Url::toRoute(['site/login']) ?>"><b><?= Yii::$app->name ?></b></a>
    </div>
    <div class="login-box-body box-body">
        <h1 class="text-center page-header"><?= Html::encode($this->title) ?></h1>

        <p class="text-center">Prosimy podać Twój adres email. Link do zresetowania hasła zostanie wysłany na niego.</p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email') ?>

            <div class="form-group">
                <?= Html::a('Powrót do logowania', ['login'], ['class' => 'btn btn-primary', 'icon' => 'arrow-left']) ?> 
                <?= Html::submitButton('Wyślij', ['class' => 'btn btn-success', 'icon' => 'send']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>