<?php

use gandh1pl\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index'], 'icon' => 'fa-users'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'icon' => 'fa-user'];
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Aktualizuj', ['update', 'id' => $model->id], ['class' => 'btn btn-primary', 'icon' => 'edit']) ?>
        <?= Html::a('Usuń', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'icon' => 'remove',
            'data' => [
                'confirm' => 'Czy jesteś pewien/a, że chcesz usunąć tego użytkownika?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Uprawnienia', ['permissions', 'id' => $model->id], ['class' => 'btn btn-warning', 'icon' => 'fire']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            // 'auth_key',
            // 'password_hash',
            'password_reset_token:boolean',
            'email:email',
            ['attribute' => 'status', 'format' => 'raw', 'value' => $model->getStatusNameHtml(false)],
            'created_at:datetime',
            'updated_at:datetime',
            ['label' => 'Uprawnienia', 'value' => call_user_func(function() use ($model) {
                $roles = Yii::$app->authManager->getRolesByUser($model->id);
                $result = '';
                foreach($roles as $role) {
                    $result .= '<span class="label label-primary">' . $role->description . '</span><br/>';
                }
                return $result;
            }), 'format' => 'html'], 
        ],
    ]) ?>

</div>
