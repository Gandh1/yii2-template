<?php

use gandh1pl\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Użytkownicy';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'icon' => 'fa-users', 'url' => '#'];
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <p>
        <?= Html::a('Dodaj użytkownika', ['user/create'], ['class' => 'btn btn-success', 'icon' => 'plus']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            ['attribute' => 'username', 'format' => 'html', 'value' => function($model) { return Html::a($model->username, ['view', 'id' => $model->id]); }],
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token:boolean',
            'email:email',
            [
                'attribute' => 'status', 
                'format' => 'raw', 
                'value' => function($model) { return $model->getStatusNameHtml(); },
                'filter' => $searchModel::$statusNames
            ],
            'created_at:datetime',
            ['label' => 'Uprawnienia', 'value' => function($model) {
                $roles = Yii::$app->authManager->getRolesByUser($model->id);
                $result = '';
                foreach($roles as $role) {
                    $result .= '<span class="label label-primary">' . $role->description . '</span><br/>';
                }
                return $result;
            }, 'format' => 'html'], 
            // 'updated_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'options' => ['class' => 'table-responsive'],
    ]); ?>

</div>
