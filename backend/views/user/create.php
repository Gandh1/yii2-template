<?php

use gandh1pl\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Dodaj użytkownika';
$this->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index'], 'icon' => 'fa-users'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'icon' => 'plus'];
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
