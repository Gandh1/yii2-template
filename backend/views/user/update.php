<?php

use gandh1pl\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserForm */

$this->title = 'Aktualizuj użytkownika: ' . ' ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index'], 'icon' => 'fa-users'];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => $model->id], 'icon' => 'fa-user'];
$this->params['breadcrumbs'][] = ['label' => 'Aktualizuj', 'icon' => 'edit', 'url' => '#'];
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
