<?php

use gandh1pl\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $user common\models\User */
/* @var $model backend\models\PermissionForm */

$this->title = 'Zarządzaj uprawnieniami';
$this->params['breadcrumbs'][] = ['label' => 'Użytkownicy', 'url' => ['index'], 'icon' => 'fa-users'];
$this->params['breadcrumbs'][] = ['label' => $user->username, 'url' => ['view', 'id' => $user->id], 'icon' => 'fa-user'];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'icon' => 'fire', 'url' => '#'];

$roles = [];
foreach(Yii::$app->authManager->getRoles() as $k => $v) {
    $roles[$v->name] = $v->description;
}

?><div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="callout callout-info">
        <b>Ostrożnie!</b>
        <p>Uważaj przy edycji uprawnień. Złe decyzje mogą się skończyć tym, że ktoś (albo i Ty) utracicie możliwość zalogowania się i cofnięcia zmian!</p>
    </div>

    <?php $form = ActiveForm::begin([
        
    ]) ?>
    
    <?= $form->field($model, 'roles')->widget(Select2::className(), [
        'language' => 'pl',
        'data' => $roles,
        'options' => ['placeholder' => 'Wybierz role', 'multiple' => true],
    ]) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success', 'icon' => 'save']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
