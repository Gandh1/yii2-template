<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use gandh1pl\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use gandh1pl\helpers\log\LogBehavior;
use kartik\daterange\DateRangePicker;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model common\models\? */
/* @var $searchModel common\models\?Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Przegląd zmian';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'icon' => 'fa-list-alt'];

?>
<div class="shop-service-logs">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php Pjax::begin(['id' => 'pjax-logs']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'created_at',
                // 'format' => 'datetime',
                'value' => function($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at) . ' (' . Yii::$app->formatter->asRelativeTime($model->created_at) . ')';
                },
                'filter' => '<div class="input-group">'
                    . '<span class="input-group-btn">
                        <button class="btn btn-default btn-clear-value" type="button">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </span>'
                    .DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'convertFormat'=>true,
                        'presetDropdown'=>true,
                        'pluginOptions'=>[
                            'locale'=>[
                                'format'=>'Y-m-d',
                            ],
                            'showDropdowns'=>true,
                            'linkedCalendars' => false
                        ]
                    ])
                . '</div>'
            ],
            'user0.username:text:Użytkownik',
            [
                'attribute' => 'action',
                'filter' => false,
                'value' => function($model) {
                    $options = ['class' => 'label'];
                    if($model->action === LogBehavior::LOG_ACTION_INSERT) {
                        Html::addCssClass($options, 'label-success');
                        $options['icon'] = 'plus';
                        $label = 'Utworzenie';
                    } elseif($model->action === LogBehavior::LOG_ACTION_UPDATE) {
                        Html::addCssClass($options, 'bg-blue');
                        $options['icon'] = 'edit';
                        $label = 'Aktualizacja';
                    } elseif($model->action === LogBehavior::LOG_ACTION_DELETE) {
                        Html::addCssClass($options, 'label-danger');
                        $options['icon'] = 'trash';
                        $label = 'Usunięcie';
                    } else {
                        $options['icon'] = 'question-sign';
                        $label = 'Inna akcja';
                    }
                    return Html::tag('span', $label, $options);
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'data',
                'format' => 'raw',
                'filter' => false,
                'value' => function($m) use ($model) {
                    $this->beginBlock('modal');
                    Modal::begin([
                        'header' => '<h2>Przegląd zmiany</h2>',
                        'headerOptions' => ['class' => 'text-center'],
                        'toggleButton' => ['label' => Html::icon('search') . ' Zobacz dane', 'class' => 'btn btn-sm btn-default'],
                        'size' => 'modal-lg'
                    ]);
                    echo $this->render('//logs/details', [
                        'model' => $m,
                        'contextModel' => $model,
                        'formats' => [
                            'created_at' => 'datetime',
                            'updated_at' => false,
                        ]
                    ]);
                    Modal::end();
                    $this->endBlock('modal');
                    return $this->blocks['modal'];
                }
            ]
        ]
    ]) ?>
    <?php Pjax::end(); 
    $this->registerJs('
        $(document).on("click", "tr.filters .btn-clear-value", function() {
            $(this).parent().next("input").val("").trigger("change");
        });
    ');
    ?>
</div>