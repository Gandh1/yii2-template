<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

use gandh1pl\helpers\Html;
use gandh1pl\helpers\log\LogBehavior;

/* @var $this yii\web\View */
/* @var $model gandh1pl\helpers\log\Logs */
/* @var $contextModel yii\db\ActiveRecord */
/* @var $formats array */

$values = json_decode($model->data, true);
?>

<div class="table-responsive">
    <table class="table table-condensed">
        <tr>
            <th class="text-right">Typ akcji</th>
            <th class="text-center">Użytkownik</th>
            <th>Data wykonania</th>
        </tr>
        <tr>
            <td class="text-right"><?php 
                $options = ['class' => 'label'];
                if($model->action === LogBehavior::LOG_ACTION_INSERT) {
                    Html::addCssClass($options, 'label-success');
                    $options['icon'] = 'plus';
                    $label = 'Utworzenie';
                } elseif($model->action === LogBehavior::LOG_ACTION_UPDATE) {
                    Html::addCssClass($options, 'bg-blue');
                    $options['icon'] = 'edit';
                    $label = 'Aktualizacja';
                } elseif($model->action === LogBehavior::LOG_ACTION_DELETE) {
                    Html::addCssClass($options, 'label-danger');
                    $options['icon'] = 'trash';
                    $label = 'Usunięcie';
                } else {
                    $options['icon'] = 'question-sign';
                    $label = 'Inna akcja';
                }
                echo Html::tag('span', $label, $options);
            ?></td>
            <td class="text-center"><?= $model->user0->username ?></td>
            <td><?= Yii::$app->formatter->asDatetime($model->created_at) ?></td>
        </tr>
    </table>
    <hr/>
    <table class="table table-hover table-condensed">
        <tr>
            <th class="text-right">Nazwa</th>
            <?php if($model->action === LogBehavior::LOG_ACTION_UPDATE): ?>
                <th class="text-center">Przed zmianą</th>
                <th>Po zmianie</th>
            <?php else: ?>
                <th>Wartość</th>
            <?php endif; ?>
        </tr>
        <?php foreach($values as $key => $value):
            $format = $formats[$key] ?? null;
            if($format === false) continue;
        ?>
        <tr>
            <td class="text-right"><b><?= $contextModel->getAttributeLabel($key) ?></b></td>
            <?php if($model->action === LogBehavior::LOG_ACTION_UPDATE): ?>
                <td class="text-center"><?php
                    if($format !== null) {
                        if(is_string($format)) {
                            echo Yii::$app->formatter->format($value['from'], $format);
                        } elseif(is_callable($format)) {
                            echo call_user_func_array($format, [$value['from'], $key, $model, $contextModel]);
                        } else {
                            throw new \yii\base\InvalidParamException('Nieznany rodzaj formatowania');
                        }
                    } else {
                        echo empty($value['from']) ? Yii::$app->formatter->nullDisplay : $value['from'];
                    }
                ?></td>
                <td><?php
                    if($format !== null) {
                        if(is_string($format)) {
                            echo Yii::$app->formatter->format($value['to'], $format);
                        } elseif(is_callable($format)) {
                            echo call_user_func_array($format, [$value['to'], $key, $model, $contextModel]);
                        } else {
                            throw new \yii\base\InvalidParamException('Nieznany rodzaj formatowania');
                        }
                    } else {
                        echo empty($value['to']) ? Yii::$app->formatter->nullDisplay : $value['to'];
                    }
                ?></td>
            <?php else: ?>
                <td><?php
                    if($format !== null) {
                        if(is_string($format)) {
                            echo Yii::$app->formatter->format($value, $format);
                        } elseif(is_callable($format)) {
                            echo call_user_func_array($format, [$value, $key, $model, $contextModel]);
                        } else {
                            throw new \yii\base\InvalidParamException('Nieznany rodzaj formatowania');
                        }
                    } else {
                        echo empty($value) ? Yii::$app->formatter->nullDisplay : $value;
                    }
                ?></td>
            <?php endif; ?>
        </tr>
        <?php endforeach; ?>
    </table>
</div>