<?php

use gandh1pl\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $key array */

$this->title = 'Zmień ustawienia';

$this->params['breadcrumbs'][] = ['label' => 'Ustawienia', 'icon' => 'fa-cogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $this->title, 'icon' => 'edit', 'url' => '#'];
?>
<div class="settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'key' => $key
    ]) ?>

</div>
