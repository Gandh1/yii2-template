<?php

use gandh1pl\helpers\Html;
use yii\grid\GridView;
use common\models\Settings;
use yii\data\ArrayDataProvider;
use yii\jui\JuiAsset;

JuiAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingsSearch */
/* @var $data array */
/* @var $unused Array */

$this->title = 'Ustawienia';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => '#', 'icon' => 'fa-cogs'];

$blocks = [
    [], []
];
$i = 0;
?>
<div class="settings-index">

    <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    <div class="form-group pull-right">
        <?= Html::a('Wyczyść pamięć podręczną', ['flush-cache'], ['class' => 'btn btn-danger', 'icon' => 'trash', 'data-confirm' => 'Czy jesteś pewien, że chcesz to zrobić?', 'data-method' => 'post']) ?>
    </div>
    <div class="clearfix"></div>
    <div class="row"><?php foreach($data as $group => $arr): if(empty($arr)) continue; ?>
        <?php $this->beginBlock('setting-' . $group); ?>
            <div class="panel panel-primary" id="block-<?= $group ?>">
                <div class="panel-heading"><?=
                    Html::a(Settings::getValue('groups', $group), '#collapse-' . $group, ['data-toggle' => 'collapse', 'icon' => 'resize-vertical'])
                ?></div>
                <div id="collapse-<?= $group ?>" class="panel-collapse collapse in">
                    <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => new ArrayDataProvider([
                            'allModels' => $arr
                        ]),
                        //'filterModel' => $searchModel,
                        'columns' => [
                            // ['class' => 'yii\grid\SerialColumn'],

                            ['attribute' => 'description', 'label' => 'Opis', 'format' => 'html', 'value' => function($model) { return Html::a($model['description'], ['edit', 'id' => $model['key']]); }],
                            ['attribute' => 'value', 'label' => 'Wartość', 'format' => 'raw', 'value' => function($model) {
                                if(isset($model['truncate'])) {
                                    return \yii\helpers\StringHelper::truncateWords($model['value'], $model['truncate'], '...', true);
                                } else if(isset($model['columnFormat'])) {
                                    if($model['columnFormat'] instanceof \Closure) {
                                        return call_user_func($model['columnFormat'], $model);
                                    } else if(is_array($model['columnFormat'])) { 
                                        return isset($model['columnFormat'][$model['value']]) ? $model['columnFormat'][$model['value']] : Yii::$app->formatter->nullDisplay;
                                    } else if(($func = Settings::getValue('columnFormat', $model['columnFormat'])) instanceof \Closure) {
                                        return call_user_func($func, $model);
                                    }
                                    return Yii::$app->formatter->format($model['value'], $model['columnFormat']);
                                }
                                return $model['value'];
                            }],

                        ],
                        'layout' => "{items}\n{pager}",
                        'options' => ['class' => 'table-responsive'],
                    ]); ?>
                    </div>
                </div>
            </div>
        <?php $this->endBlock(); 
        $blocks[$i++][] = $this->blocks['setting-'.$group];
        if($i > 1) { $i = 0; }
        endforeach;
        ?>
        <div class="col-xs-12 col-md-6 connectedSortable columnLeft"><?php
            foreach($blocks[0] as $block) {
                echo $block;
            }
        ?></div>
        <div class="col-xs-12 col-md-6 connectedSortable columnRight"><?php
            foreach($blocks[1] as $block) {
                echo $block;
            }
        ?></div>
    </div> <!-- row -->
    <?php
        if(!empty($unused)) {
            echo '<hr/><div class="alert alert-warning">'.
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.
                Html::icon('wrench') . ' <b>Uwaga!</b> Istnieje <b>' . count($unused) . '</b> nieużywanych ustawień w bazie danych!'
            .'</div>';
        }
        $this->registerJs('
            var sortable = $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".panel-heading",
                forcePlaceholderSize: true,
                zIndex: 9999
            });
            sortable.find(".panel .panel-heading").css("cursor", "move");
            
            if(localStorage !== undefined) {
                sortable.on("sortupdate", function(event, ui) {
                    var sorted = $(this).sortable("toArray"),
                        target = $(event.target);
                    if(target.is(".columnLeft")) {
                        localStorage.setItem("settings-col-left", sorted);
                    } else {
                        localStorage.setItem("settings-col-right", sorted);
                    }
                });
                
                function sortColumn(id, ls_index) {
                    var positions = localStorage.getItem(ls_index);
                    if(positions !== null) {
                        var pos = positions.split(",");
                        for(var i = 0; i < pos.length; i++) {
                            $("#" + pos[i]).appendTo(id);
                        }
                    }
                }
                sortColumn(".columnLeft", "settings-col-left");
                sortColumn(".columnRight", "settings-col-right");
            }
            
        ');
    ?>
</div>
