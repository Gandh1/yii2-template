<?php

use gandh1pl\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
/* @var $key array */

$description = ArrayHelper::getValue($key, 'description', '');
$hint = ArrayHelper::getValue($key, 'hint', '');

$fieldConfig = ArrayHelper::getValue($key, 'fieldConfig', [
    'function' => 'textInput',
    'params' => [
        ['maxlength' => true],
    ]
]);

if($fieldConfig instanceof \Closure) {
    $fieldConfig = $fieldConfig($model, $key);
}

?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'key')->dropDownList([$model->key => $description], ['disabled' => 'disabled']) ?>
    
    <p class="text-muted"><?= $hint ?></p>

    <?= call_user_func_array([$form->field($model, 'value'), $fieldConfig['function']], $fieldConfig['params']) ?>

    <div class="form-group">
        <?= Html::a('Powrót', ['settings/index'], ['class' => 'btn btn-primary', 'icon' => 'arrow-left']) ?>
        <?= Html::submitButton('Zapisz', ['class' => 'btn btn-success', 'icon' => 'edit']) ?>
        <?php 
            if(!$model->isNewRecord) {
                echo Html::a('Wyczyść', ['settings/delete', 'id' => $model->key], ['class' => 'btn btn-danger', 'icon' => 'remove', 'data-method' => 'post', 'data-confirm' => 'Czy jesteś pewny, że chcesz wyczyścić tą wartość?']);
            }
        ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
