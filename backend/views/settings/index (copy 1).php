<?php

use gandh1pl\helpers\Html;
use yii\grid\GridView;
use common\models\Settings;
use yii\data\ArrayDataProvider;
use common\widgets\adminlte\Box;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SettingsSearch */
/* @var $data array */
/* @var $unused Array */

$this->title = 'Ustawienia';

$this->params['breadcrumbs'][] = ['label' => $this->title, 'icon' => 'fa-cogs', 'url' => '#'];

$blocks = [
    [], []
];
$i = 0;
?>
<div class="settings-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row"><?php foreach($data as $group => $arr): ?>
        <?php $this->beginBlock('setting-' . $group); ?>
            <div class="panel panel-primary">
                <div class="panel-heading"><?=
                    Html::a(Settings::getValue('groups', $group), '#collapse-' . $group, ['data-toggle' => 'collapse', 'icon' => 'resize-vertical'])
                ?></div>
                <div id="collapse-<?= $group ?>" class="panel-collapse collapse in">
                    <div class="panel-body">
                    <?= GridView::widget([
                        'dataProvider' => new ArrayDataProvider([
                            'allModels' => $arr
                        ]),
                        //'filterModel' => $searchModel,
                        'columns' => [
                            // ['class' => 'yii\grid\SerialColumn'],

                            ['attribute' => 'description', 'label' => 'Opis', 'format' => 'html', 'value' => function($model) { return Html::a($model['description'], ['edit', 'id' => $model['key']]); }],
                            ['attribute' => 'value', 'label' => 'Wartość', 'format' => 'raw', 'value' => function($model) {
                                if(isset($model['truncate'])) {
                                    return \yii\helpers\StringHelper::truncateWords($model['value'], $model['truncate'], '...', true);
                                } else if(isset($model['columnFormat'])) {
                                    if($model['columnFormat'] instanceof \Closure) {
                                        return call_user_func($model['columnFormat'], $model);
                                    } else if(($func = Settings::getValue('columnFormat', $model['columnFormat'])) instanceof \Closure) {
                                        return call_user_func($func, $model);
                                    }
                                    return Yii::$app->formatter->format($model['value'], $model['columnFormat']);
                                }
                                return $model['value'];
                            }],

                        ],
                        'layout' => "{items}\n{pager}",
                        'options' => ['class' => 'table-responsive'],
                    ]); ?>
                    </div>
                </div>
            </div>
        <?php $this->endBlock(); 
        $blocks[$i++][] = $this->blocks['setting-'.$group];
        if($i > 1) { $i = 0; }
        endforeach;
        ?>
        <div class="col-xs-12 col-md-6"><?php
            foreach($blocks[0] as $block) {
                echo $block;
            }
        ?></div>
        <div class="col-xs-12 col-md-6"><?php
            foreach($blocks[1] as $block) {
                echo $block;
            }
        ?></div>
    </div> <!-- row -->
    <?php
        /*if(!empty($unused)) {
            echo '<hr/><div class="alert alert-warning">'.
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.
                Html::icon('wrench') . ' <b>Uwaga!</b> Istnieje <b>' . count($unused) . '</b> nieużywanych ustawień w bazie danych!'
            .'</div>';
        }*/
    ?>
</div>
