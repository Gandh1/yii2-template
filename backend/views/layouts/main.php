<?php
use backend\assets\AppAsset;
use gandh1pl\helpers\Html;
use gandh1pl\helpers\Breadcrumbs;
use gandh1pl\helpers\Alert;
use gandh1pl\adminlte\Nav;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$counters = Yii::$app->cacheHelper->get('counters');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Panel administracyjny - <?= Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-green sidebar-mini">
    <?php $this->beginBody() ?>
     <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
		<?= Html::a('<span class="logo-mini">'.Html::img('@www/favicon.png', ['width'=>'32', 'height'=>'32']).'</span><span class="logo-lg"><b>'.Yii::$app->name.'</b></span>', 
			['site/index'],
			['class'=>'logo']
		) ?>
		
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <?= Html::a(Html::img('@www/favicon.png', ['class' => 'user-image', 'alt' => 'User image'])
                            . Yii::$app->user->identity->username, '#', [
                            'class' => 'dropdown-toggle',
                            'data-toggle' => 'dropdown'
                        ]) ?>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <?= Html::img('@www/images/logo.png', ['class' => 'img-circle', 'alt' => 'User image']) ?>
                                <p>
                                    <?= Yii::$app->user->identity->username ?>
                                    <small>Konto od <?= Yii::$app->formatter->asDatetime(Yii::$app->user->identity->created_at, 'MMM Y') ?></small>
                                </p>
                            </li>
                            <li class="user-body"></li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <p>Uprawnienia:</p>
                                    <?php $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
                                    if(empty($roles)): ?>
                                    <p class="text-danger">Brak uprawnień!</p>
                                    <?php else: foreach($roles as $role): ?>
                                    <p><span class="label label-primary"><?= $role->description ?></span></p>
                                    <?php endforeach; endif; ?>
                                </div>
                                <div class="pull-right">
                                    <?= Html::a('Wyloguj', ['/site/logout'], ['class' => 'btn btn-default btn-flat', 'icon' => 'off', 'data-method' => 'post']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
				&nbsp;
            </div>
            <div class="pull-left info">
				<?php if(Yii::$app->user->isGuest): ?>
					<p>Niezalogowany</p>
				<?php else: ?>
					<p><?= Yii::$app->user->identity->username ?></p>
					<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				<?php endif; ?>
            </div>
          </div>
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <?php
          $isLogged = !Yii::$app->user->isGuest;
          echo Nav::widget([
              'items' => [
                  ['label' => 'Zaloguj', 'url' => ['site/login'], 'visible' => !$isLogged, 'icon' => 'unlock', 'itemActiveCheckingSimple' => false],
                  ['label' => 'Strona domowa', 'url' => ['site/index'], 'visible' => $isLogged, 'icon' => 'fa-dashboard', 'itemActiveCheckingSimple' => false],
                  ['label' => 'System', 'url' => '#', 'visible' => $isLogged && Yii::$app->user->can('admin'), 'icon' => 'fa-cogs', 'items' => [
                      ['label' => 'Użytkownicy', 'url' => ['user/index'], 'icon' => 'fa-user', 'badge' => [$counters['users'], '']],
                      // ['label' => Yii::t('backend', 'Content management'), 'url' => ['content/index'], 'icon' => 'list-alt'],
                      ['label' => 'Ustawienia', 'url' => ['settings/index'], 'icon' => 'fa-cog'],
                  ]],
                  ['label' => 'Wyloguj się', 'url' => ['site/logout'], 'visible' => $isLogged, 'icon' => 'fa-lock', 'linkOptions' => ['data-method' => 'post'], 'itemActiveCheckingSimple' => false],
              ],
              'itemActiveCheckingSimple' => true,
              'options' => [
                  'class' => 'sidebar-menu',
              ]
          ]) ?>
        </section>
        <!-- /.sidebar -->
      </aside>

		<div class="content-wrapper">
			<section class="content-header">
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    'options' => ['class' => 'breadcrumb']
				]) ?>
				<h1><?= (isset($this->params['content-header']) ? $this->params['content-header'] : /*'&nbsp;'*/ '<small>'.$this->title.'</small>') ?></h1>
                <div class="clearfix"></div>
			</section>
			<section class="content">
				<?= Alert::widget([
                    			'closeButton' => false,
                ]) 		?>
				<?php
					if(!isset($this->params['content-box']) || $this->params['content-box'] !== false): 
				?><div class="box">
					<div class="box-body">
						<?= $content ?>
					</div>
				</div><?php else: 
					echo $content;
				endif; ?>
			</section>
		</div>
    </div>

	<footer class="main-footer">
		<div class="pull-right hidden-xs">
			<?= Yii::powered() ?>
		</div>
		&copy; <?= Yii::$app->name ?> <?= date('Y') ?>
	</footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
