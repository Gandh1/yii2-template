<?php
namespace backend\models;

use Yii;
use common\models\User;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'Nie istnieje użytkownik o podanym adresie email.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        if(($emailFrom = Yii::$app->settings->contactEmail) === null) {
            $this->addError('email', 'Przepraszamy, nie możemy wysłać emaila z linkiem do resetowania hasła w tej chwili. Administrator nie skonfigurował odpowiednio tej usługi.');
            return false;
        }
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }

            if ($user->save()) {
                return \Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user])
                    ->setFrom([$emailFrom => \Yii::$app->name . ' robot'])
                    ->setTo($this->email)
                    ->setSubject('Odzyskiwanie hasła - ' . \Yii::$app->name)
                    ->send();
            }
        }

        return false;
    }
}
