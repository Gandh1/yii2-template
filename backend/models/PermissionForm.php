<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\models;

use Yii;

/**
 * Description of PermissionForm
 *
 * @author Rafal Woloszka
 */
class PermissionForm extends \yii\base\Model {
    
    public $roles = [];
    
    public function rules() {
        return [
            ['roles', 'safe']
        ];
    }
    
    public function attributeLabels() {
        return [
            'roles' => 'Uprawnienia'
        ];
    }
    
    public function assign($user) {
        $auth = Yii::$app->authManager;
        
        $auth->revokeAll($user->id);
        
        if(!empty($this->roles)) {
            foreach($this->roles as $roleName) {
                $role = $auth->getRole($roleName);
                if($role !== null) {
                    $auth->assign($role, $user->id);
                    $auth->invalidateCache();
                }
            }
        }
        
        $user->generateAuthKey();
        return $user->save(false);
    }
}
