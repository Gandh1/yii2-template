<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace backend\assets;

/**
 * Description of AdminLTEAsset
 *
 * @author Rafal Woloszka
 */
class AdminLTEAsset extends \yii\web\AssetBundle {
    
    public $sourcePath = '@vendor/almasaeed2010/adminlte/dist';
    public $css = [
		'css/AdminLTE.min.css', 
		'css/skins/_all-skins.min.css',
		'https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css',
		'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css'
	];
    public $js = ['js/app.min.js'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
