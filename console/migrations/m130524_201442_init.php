<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),

            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'last_visit' => $this->integer()
        ], $tableOptions);
        
        $this->createTable('{{%settings}}', [
            'key' => $this->string(50),
            'value' => $this->string(255)->notNull(),
        ]);
        $this->addPrimaryKey('settings_pk', '{{%settings}}', 'key');
        
        $this->createTable('{{%content}}', [
            'key' => $this->string(127)->notNull(),
            'type' => $this->string(255)->notNull(),
            'content' => $this->text(),
            'parent' => $this->string(127)
        ]);
        $this->addPrimaryKey('content_pk', '{{%content}}', 'key');
        $this->createIndex('content_parent', '{{%content}}', 'parent');
        $this->addForeignKey('fk_content_parent', '{{%content}}', ['parent'], '{{%content}}', ['key'], 'RESTRICT', 'CASCADE');
        
        /** 
         * TAGI 
         */
        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(127)->notNull(),
            'frequency' => $this->integer()->notNull()->defaultValue(1)
        ]);
        
        $this->createTable('{{%log_actions}}', [
            'id' => $this->primaryKey(),
            'user' => $this->integer(),
            'context_type' => $this->string(127)->notNull(),
            'context' => $this->string(255),
            'action_type' => $this->integer()->notNull(),
            'timestamp' => $this->integer()->notNull(),
            'action_params' => $this->text()
        ]);
        $this->createIndex('log_actions_user', '{{%log_actions}}', 'user');
        $this->createIndex('log_actions_context', '{{%log_actions}}', 'context');
        $this->createIndex('log_actions_action_type', '{{%log_actions}}', 'action_type');
        $this->addForeignKey('fk_log_actions_user', '{{%log_actions}}', ['user'], '{{%user}}', ['id'], 'SET NULL', 'CASCADE');
        
        /**
         * SYSTEM GALERII
         */
        $this->createTable('{{%gallery_custom}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(127),
            'description' => $this->string(255),
            'image_path' => $this->string(255)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'position' => $this->integer()->defaultValue(0)
        ]);
        
        $this->createTable('{{%gallery_custom_tag}}', [
            'gallery_custom_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_gallery_custom_tag_gallery_custom_id', '{{%gallery_custom_tag}}', ['gallery_custom_id'], '{{%gallery_custom}}', ['id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_gallery_custom_tag_tag_id', '{{%gallery_custom_tag}}', ['tag_id'], '{{%tag}}', ['id'], 'CASCADE', 'CASCADE');
        
        $this->createTable('{{%gallery}}', [
            'id' => $this->primaryKey(),
            'context_type' => $this->integer()->notNull(),
            'context_pk' => $this->string(255),
            'title' => $this->string(255),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()
        ]);
        $this->createIndex('idx_gallery_context_type', '{{%gallery}}', ['context_type']);
        $this->createIndex('idx_gallery_context_pk', '{{%gallery}}', ['context_pk']);
        
        $this->createTable('{{%gallery_image}}', [
            'id' => $this->primaryKey(),
            'gallery_id' => $this->integer()->notNull(),
            'title' => $this->string(127),
            'description' => $this->text(),
            'image' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'position' => $this->integer()->notNull(),
        ]);
        $this->addForeignKey('fk_gallery_image_gallery_id', '{{%gallery_image}}', ['gallery_id'], '{{%gallery}}', ['id'], 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        // System galerii
        $this->dropTable('{{%gallery_custom}}');
        
        $this->dropForeignKey('fk_gallery_custom_tag_tag_id', '{{%gallery_custom_tag}}');
        $this->dropForeignKey('fk_gallery_custom_tag_gallery_custom_id', '{{%gallery_custom_tag}}');
        $this->dropTable('{{%gallery_custom_tag}}');
        
        $this->dropForeignKey('fk_gallery_image_gallery_id', '{{%gallery_image}}');
        $this->dropTable('{{%gallery_image}}');
        $this->dropTable('{{%gallery}}');
        
        // Tagi
        $this->dropTable('{{%tag}}');
        
        $this->dropForeignKey('fk_content_parent', '{{%content}}');
        $this->dropTable('{{%content}}');
        
        $this->dropTable('{{%settings}}');
        
        $this->dropForeignKey('fk_log_actions_user', '{{%log_actions}}');
        $this->dropTable('{{%log_actions}}');
        
        $this->dropTable('{{%user}}');
    }
}
