<?php

/*
The MIT License

Copyright 2016 Rafal Woloszka.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

namespace console\controllers;

use Yii;
use common\models\User;

/**
 * Description of RbacController
 *
 * @author Rafal Woloszka
 */
class RbacController extends \yii\console\Controller {
    
    protected $auth = null;
    
    public function init() {
        parent::init();
        
        if(!(Yii::$app->authManager instanceof yii\rbac\BaseManager)) {
            throw new InvalidConfigException('You must configure authManager component to use RBAC.');
        }
        $this->auth = Yii::$app->authManager;
    }
    
    protected function permission($id, $description, $ruleName = null) {
        $perm = $this->auth->createPermission($id);
        $perm->description = $description;
        
        if($ruleName !== null) {
            $perm->ruleName = $ruleName;
        }
        
        $this->auth->add($perm);
        
        return $perm;
    }
    
    protected function role($id, $description, array $childs) {
        $role = $this->auth->createRole($id);
        $role->description = $description;
        $this->auth->add($role);
        
        foreach($childs as $child) {
            $this->auth->addChild($role, $child);
        }
        return $role;
    }
    
    protected function rule($rule) {
        $this->auth->add($rule);
        return $rule;
    }
    
    public function actionInit($reassign = true)
    {
        $oldRoles = [];
        $auth = $this->auth;
        
        if($reassign) {
            foreach($auth->getRoles() as $roleName => $role) {
                $oldRoles[$roleName] = $auth->getUserIdsByRole($roleName);
            }
        }
        
        $auth->removeAll();
        
        $loginPermission = $auth->createPermission('loginPermission');
        $loginPermission->description = 'Możliwość logowania się';
        $auth->add($loginPermission);
        
        $settingsManagement = $auth->createPermission('settingsManagement');
        $settingsManagement->description = 'Zarządzanie ustawieniami';
        $auth->add($settingsManagement);
        
        $adminRole = $auth->createRole('admin');
        $adminRole->description = 'Administrator';
        $auth->add($adminRole);
        $auth->addChild($adminRole, $settingsManagement);
        $auth->addChild($adminRole, $loginPermission);
        
        foreach($oldRoles as $oldRoleName => $userIDs) {
            if(($oldRole = $auth->getRole($oldRoleName)) !== null) {
                foreach($userIDs as $userID) {
                    $auth->assign($oldRole, $userID);
                }
            }
        }
        
        echo "RBAC poprawnie skonfigurowany.\n";
        exit(0);
    }
    
    public function actionClear() {
        $this->auth->removeAll();
    }
    
    public function actionAssign($userName, $roleName, $revoke = true) {
        $role = $this->auth->getRole($roleName);
        if($role === null) {
            echo "Rola $roleName nie zostala odnaleziona.\n";
            exit(1);
        }
        
        $model = User::findOne(['username' => $userName]);
        if($model === null) {
            echo "Uzytkownik $userName nie istnieje.\n";
            exit(2);
        }
        try {
            if($revoke) {
                $this->auth->revokeAll($model->id);
            }
            $this->auth->assign($role, $model->id);
            $this->auth->invalidateCache();
            echo "Poprawnie przypisano uzytkownikowi " . $userName . " uprawnienie $roleName.\n";
        } catch(yii\base\Exception $ex) {
            echo "Nastąpił błąd przy dodawaniu roli $roleName dla użytkownika $userName:\nPrawdopodobnie ten użytkownik już posiada podane uprawnienie.\n";
        }
    }
}
