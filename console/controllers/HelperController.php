<?php

/*
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace console\controllers;

use common\models\User;

/**
 * Description of HelperController
 *
 * @author Rafal Woloszka
 */
class HelperController extends \yii\console\Controller {
    
    protected function createUserValidator($model, $attribute) {
        return function($input, &$error) use($model, $attribute) {
            $model->$attribute = $input;
            if(!$model->validate([$attribute])) {
                $error = $model->getFirstError($attribute);
                return false;
            }
            return true;
        };
    }
    
    public function actionUserAdd() {
        
        $model = new User;
        $model->createActivated = true;
        $model->status = User::STATUS_ACTIVE;
        $model->generateAuthKey();
        
        $model->username = $this->prompt('Podaj nazwę użytkownika: ', [
            'required' => true,
            'validator' => $this->createUserValidator($model, 'username')
        ]);
        
        $password = $this->prompt('Podaj hasło: ', [
            'required' => true,
            'validator' => $this->createUserValidator($model, 'password')
        ]);
        $model->setPassword($password);
        
        $model->email = $this->prompt('Podaj adres email: ', [
            'required' => true,
            'validator' => $this->createUserValidator($model, 'email')
        ]);
        
        if($model->save(false)) {
            echo "Poprawnie dodano nowego użytkownika. Aby nadać mu uprawnienia, użyj komendy yii rbac/assign {username} {role}.\n";
            exit(0);
        } else {
            echo "Wystąpił błąd: " . current($model->getFirstErrors()) . "\n";
            exit(1);
        }
    }
}
