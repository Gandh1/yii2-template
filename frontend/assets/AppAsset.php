<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@wwwroot';
    public $baseUrl = '@www';
    public $css = [
        'css/site.css',
    ];
    public $js = [
	'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        // 'yii\bootstrap\BootstrapPluginAsset',
        'common\assets\SharedAsset',
        // 'frontend\assets\OnePageNav',
        // 'frontend\assets\HoverAsset',
        // 'frontend\assets\WowAsset',
        'common\assets\SweetAlertAsset',
    ];
}
