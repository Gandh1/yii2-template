/* 
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$.fn.scrollHere = function(duration, gap) {
    var $this = $(this), duration = (typeof duration == 'undefined' ? 900 : duration), gap = (typeof gap == 'undefined' ? 0 : gap);
    $('html, body').stop().animate({
            'scrollTop': ($this.offset()) ? ($this.offset().top - gap) : 0
        }, duration, 'swing'
    );
    return $this;
};

$(window).load(function() {
    $(".same-height").sameHeight();
    $(".valign-middle").valignMiddle();
    
    /* $('.navbar').onePageNav({
        navItems: '.navbar-nav > li > a',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.25,
        currentClass: 'active',
        filter: ':not(.external):not(.inpage-scroll):not([tabindex])',
        allowSameSection: true
    }); */
    
    $('a[href^="#"].inpage-scroll, .inpage-scroll a[href^="#"], a[data-scroll^="#"]').on('click', function(e) {
        e.preventDefault();
        
        var target = $(this).data('scroll');
        if(!target) {
            target = this.hash;
        }
        var $target = $(target);
        
        $('.main-navigation a[href="' + target + '"]').addClass('active');
        $('.main-navigation a:not([href="' + target + '"])').removeClass('active');
        $('html, body').stop().animate({
            'scrollTop': ($target.offset()) ? $target.offset().top - $("nav").height() : 0
        }, 750, 'swing');
        
        //return false;
    });
    
    /* var wow = new WOW({
        animateClass: 'animated',
        offset: 100,
        callback: function(box) {
            // console.log($(box).hasClass('woow'));
            if(!$(box).hasClass('woow')) {
                $(box).find(".woow").each(function() {
                    wow.show(this);
                });
            }
        }
    });
    wow.init();
    $(".woow").each(function() {
        wow.applyStyle(this, true);
    }); */
    
    $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});
    
    /* var topButton = $('.scroll-top');
    if(topButton.length !== 0) {
        var offsetStartScrollTop = $('#o-nas').offset().top;
        $(window).scroll(function() {
            if($(this).scrollTop() > offsetStartScrollTop) {
                topButton.filter('.hide').removeClass("hide fadeOutDown").anim('fadeInUp', false);
            } else {
                topButton.filter(':not(.hide)').removeClass("fadeInUp").anim('fadeOutDown', false, function($obj) {
                    $obj.addClass("hide");
                });
            }
        });
        if($(window).scrollTop() >= offsetStartScrollTop) {
            topButton.removeClass("hide");
        }
    } */
    
    $(".facebook-widget").bind('mouseenter focusin', function(e){
		if(e.target.tagName == "IMG") {
			$(".facebook-widget").stop().animate({"right": "0", "easing":"easeInOutQuad"}, 750); 
			$(".facebook-widget img").stop().animate({"opacity":0}, 750);
		}
	}).bind("mouseleave focusout", function(e){ 
		$(".facebook-widget").stop().animate({"right": "-299px", "easing":"easeInOutQuad"}, 750); 
		$(".facebook-widget img").stop().animate({"opacity":1}, 750);
	});
});