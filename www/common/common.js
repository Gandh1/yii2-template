/* 
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$.fn.sameHeight = function(context) {
    $(this).each(function() {
        var $this = $(this);
        context = context || $this.data('context');

        var heights = $this.find(context).map(function() {
            return $(this).height();
        }).get(),
            maxHeight = Math.max.apply(null, heights);

        $this.find(context).height(maxHeight);
    });
};
$.fn.valignMiddle = function() {
    $(this).each(function() {
        var contentHeight = 0, height = $(this).height();
        $(this).children().each(function() {
            contentHeight += $(this).outerHeight(true);
        });

        var diff = (height - contentHeight) / 2;
        $(this).css({'padding-top': diff, 'padding-bottom': diff});
    });
};
