/*
 * The MIT License
 *
 * Copyright 2016 Rafal Woloszka.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

$.fn.noTransition = function(delay) {
    var $this = this;
    $this.addClass('no-transition');
    if(delay !== undefined) {
        setTimeout(function() {
            $this.removeClass('no-transition');
        }, delay);
    } else {
        $this.one('transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd', function() {
            $(this).removeClass('no-transition');
        });
    }
};

$.AdminLTE.options.animationSpeed = 300;
$.AdminLTE.options.BSTooltipSelector = '';
$(function() {
    "use strict";
    
    yii.confirm = function(message, ok, cancel) {
        swal({
            title: "",
            text: message,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Ok",
            confirmButtonColor: '#DD6B55',
            cancelButtonText: "Anuluj",
            closeOnConfirm: false
        }, function(isConfirm) {
            if(isConfirm) {
                !ok || ok();
            } else {
                !cancel || cancel();
            }
        });
    };
    
    // $.AdminLTE.pushMenu("[data-toggle='offcanvas']");
    
    if(window.localStorage) {
        var state = window.localStorage.getItem('sidebar-state');
        var screenSizes = $.AdminLTE.options.screenSizes;
        if(state === "expanded") {
            // TOOD?
        } else if(state === "collapsed") {
            $(".main-sidebar, .content-wrapper, .logo, .navbar").noTransition(300); // hack
            $("body").addClass("sidebar-collapse");
        }
        
        $("body").on("expanded.pushMenu collapsed.pushMenu", function(e) {
            window.localStorage.setItem("sidebar-state", e.type);
        });
    }
    
    var initAfterRefresh = function() {
        $('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
    };
    initAfterRefresh();
    $(document).on('pjax:complete', initAfterRefresh);
    
    $(".alert.alert-notify").each(function() {
        $.notify({
            message: $(this).html()
        }, {
            type: $(this).data("type")
        });
    });
});