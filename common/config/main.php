<?php
return [
    'name'=>'Yii2',
    'language'=>'pl',
    'timezone'=>'Europe/Warsaw',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'cache'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/cache'
        ],
        'settings' => [
            'class' => 'gandh1pl\helpers\SettingsComponent',
            'modelNamespace' => "common\models\Settings",
            'modelPath' => '@common/models/Settings.php'
        ],
        'formatter' => [
            'sizeFormatBase' => 1000,
            // 'datetimeFormat' => 'short'
        ],
        'cacheHelper' => [
            'class' => 'gandh1pl\helpers\CacheHelper',
            'cacheKeys' => [
                'counters' => function() {
                    $modelsCountersCache = [
                        'users' => 'common\models\User',
                    ];
                    
                    $counters = [];
                    foreach($modelsCountersCache as $key => $modelClassname) {
                        $counters[$key] = intval($modelClassname::find()->count());
                    }
                    return $counters;
                },
                'latests' => [],
            ]
        ]
    ],
];
