<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'uploadPath' => '@wwwroot/files/uploads/',
    'uploadUrl' => '@www/files/uploads/',
];
