<?php

use yii\base\Event;
use gandh1pl\helpers\Html;

Yii::setAlias('www', '/');
Yii::setAlias('wwwroot', dirname(dirname(__DIR__)) . '/www');
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

Event::on(yii\web\View::className(), yii\web\View::EVENT_BEGIN_BODY, function($event) {
    $event->sender->registerLinkTag([
        'rel' => 'icon', 
        'type' => 'image/png', 
        'href' => Yii::getAlias('@www/favicon.png')
    ]);
    $event->sender->registerMetaTag([
        'name' => 'description',
        'content' => Yii::$app->settings->metaDescription
    ]);
    $event->sender->registerMetaTag([
        'name' => 'keywords',
        'content' => Yii::$app->settings->metaKeywords
    ]);
});

Event::on(\yii\web\User::className(), \yii\web\User::EVENT_AFTER_LOGIN, function($event) {
    $user = $event->sender->identity;
    
    $user->updateAttributes(['last_visit' => time()]);
});

Event::on(\yii\web\Application::className(), \yii\web\Application::EVENT_BEFORE_REQUEST, function($event) {
    Yii::$app->name = Yii::$app->settings->siteName;
});

\Yii::$container->set('yii\grid\GridView', [
    'options' => ['class' => 'table-responsive'],
]);
\Yii::$container->set('yii\grid\ActionColumn', [
    'buttons' => [
        'view' => function ($url, $model, $key) {
            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                'title' => Yii::t('yii', 'View'),
                'aria-label' => Yii::t('yii', 'View'),
                'data-pjax' => '0',
                'class' => 'btn btn-xs btn-primary',
                'data-toggle' => 'tooltip',
            ]);
        },
        'update' => function ($url, $model, $key) {
            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                'title' => Yii::t('yii', 'Update'),
                'aria-label' => Yii::t('yii', 'Update'),
                'data-pjax' => '0',
                'class' => 'btn btn-xs btn-success',
                'data-toggle' => 'tooltip',
            ]);
        },
        'delete' => function ($url, $model, $key) {
            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                'title' => Yii::t('yii', 'Delete'),
                'aria-label' => Yii::t('yii', 'Delete'),
                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                'data-method' => 'post',
                'data-pjax' => '0',
                'class' => 'btn btn-xs btn-danger',
                'data-toggle' => 'tooltip',
            ]);
        }
    ]
]);

\Yii::$container->set('dosamigos\tinymce\TinyMce', [
    'options' => ['style' => 'height: 400px'],
    'language' => 'pl',
    'clientOptions' => [
        'plugins' => [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        'toolbar1' => 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fullscreen',
        'toolbar2' => 'preview media | forecolor backcolor emoticons | sizeselect | bold italic | fontselect |  fontsizeselect',
        'image_advtab' => true,
        'fontsize_formats' => "5pt 6pt 7pt 8pt 10pt 12pt 14pt 18pt 24pt 36pt",
        'convert_urls' => false
    ]
]);