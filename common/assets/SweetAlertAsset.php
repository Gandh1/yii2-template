<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Description of VegasAsset
 *
 * @author Rafal Woloszka
 */
class SweetAlertAsset extends AssetBundle {
    public $sourcePath = '@bower/sweetalert/dist';
    public $css = [
        'sweetalert.css'
    ];
    public $js = [
        'sweetalert.min.js'
    ];
}
