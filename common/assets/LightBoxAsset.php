<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Description of VegasAsset
 *
 * @author Rafal Woloszka
 */
class LightBoxAsset extends AssetBundle {
    public $sourcePath = '@bower/lightbox2/dist';
    public $css = [
        'css/lightbox.min.css',
    ];
    public $js = [
        'js/lightbox.min.js'
    ];
}
