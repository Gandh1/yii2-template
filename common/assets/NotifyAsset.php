<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets;

use yii\web\AssetBundle;

class NotifyAsset extends AssetBundle
{
    public $sourcePath = '@bower/remarkable-bootstrap-notify';
    public $js = [
        'bootstrap-notify.min.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
