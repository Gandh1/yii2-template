<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use gandh1pl\helpers\Html;
use common\models\Shop;
use yii\caching\TagDependency;
use yii\filters\RateLimitInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface, RateLimitInterface
{
    const STATUS_DELETED        = 0;
    const STATUS_NOT_ACTIVATED  = 1;
    const STATUS_ACTIVE         = 10;
    
    public static $statusNames = [
        self::STATUS_DELETED        => 'Konto usunięte',
        self::STATUS_NOT_ACTIVATED  => 'Konto niezweryfikowane',
        self::STATUS_ACTIVE         => 'Konto aktywne'
    ];
    
    protected $_createActivated = false;
    public function setCreateActivated($value) { $this->_createActivated = $value; }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }
    
    public function getStatusName() {
        return self::$statusNames[$this->status];
    }
    
    public function getStatusNameHtml($block = true) {
        $options = ['class' => 'btn'];
        if($block) { Html::addCssClass($options, 'btn-block'); }
        $style = '';
        switch($this->status) {
            case self::STATUS_DELETED: $style = 'btn-red'; break;
            case self::STATUS_NOT_ACTIVATED: $style = 'btn-warning'; break;
            case self::STATUS_ACTIVE: $style = 'btn-success'; break;
        }
        Html::addCssClass($options, $style);
        return Html::tag('button', $this->statusName, $options);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status'], 'integer'],
            
            [['username', 'email'], 'required'],
            
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ta nazwa użytkownika jest już zajęta.'],
            
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Ten adres email jest już zajęty.'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => 'Nazwa użytkownika',
            'auth_key' => Yii::t('app', 'Klucz uwierzytelniający'),
            'password_hash' => Yii::t('app', 'Hash hasła'),
            'password_reset_token' => Yii::t('app', 'Token resetu hasła'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Data utworzenia'),
            'updated_at' => Yii::t('app', 'Data aktualizacji'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * Finds user by username or email
     *
     * @param string $usernameOrEmail User's name or user's email
     * @return static|null
     */
    public static function findByUsernameOrEmail($usernameOrEmail)
    {
        return static::find()->where(['status' => self::STATUS_ACTIVE])->andWhere(['or', ['username' => $usernameOrEmail], ['email' => $usernameOrEmail]])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if(($model = static::findOne([
            'password_reset_token' => $token,
            'status' => [self::STATUS_NOT_ACTIVATED, self::STATUS_ACTIVE]
        ])) !== null) {
            if($model->status === self::STATUS_ACTIVE && !static::isPasswordResetTokenValid($token)) {
                return null;
            }
            return $model;
        }
        return null;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            $this->generateAuthKey();
            
            if($insert && !$this->_createActivated) {
                $this->setPassword(rand());
                $this->status = self::STATUS_NOT_ACTIVATED;
                
                if (!User::isPasswordResetTokenValid($this->password_reset_token)) {
                    $this->generatePasswordResetToken();
                }
                
                return Yii::$app
                    ->mailer
                    ->compose(
                        ['html' => 'createAccount-html', 'text' => 'createAccount-text'],
                        ['user' => $this]
                    )
                    ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                    ->setTo($this->email)
                    ->setSubject('['.Yii::$app->name.'] Utworzono konto')
                    ->send();
            }
            return true;
        }
        return false;
    }

    public function getRateLimit($request, $action) {
        return [Yii::$app->settings->rateLimit, Yii::$app->settings->rateLimitPeriod];
    }

    public function loadAllowance($request, $action) {
        $cacheLoaded = Yii::$app->cache->get(['rateLimit', $this->id]);
        return $cacheLoaded ? $cacheLoaded : [Yii::$app->settings->rateLimit, 0];
    }

    public function saveAllowance($request, $action, $allowance, $timestamp) {
        Yii::$app->cache->set(['rateLimit', $this->id], [$allowance, $timestamp]);
    }

}
