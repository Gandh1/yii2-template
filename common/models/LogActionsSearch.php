<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LogActions;

/**
 * LogActionsSearch represents the model behind the search form about `\common\models\LogActions`.
 */
class LogActionsSearch extends LogActions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user', 'context_type', 'context', 'action_type', 'timestamp'], 'integer'],
            [['action_params'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LogActions::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->orderBy = ['id' => SORT_DESC];
        $query->andFilterWhere([
            'id' => $this->id,
            'user' => $this->user,
            'context_type' => $this->context_type,
            'context' => $this->context,
            'action_type' => $this->action_type,
            'timestamp' => $this->timestamp,
        ]);

        $query->andFilterWhere(['like', 'action_params', $this->action_params]);

        return $dataProvider;
    }
}
