<?php

namespace common\models;

use Yii;
use yii\base\UnknownPropertyException;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "log_actions".
 *
 * @property integer $id
 * @property integer $user
 * @property integer $context_type
 * @property string $context
 * @property integer $action_type
 * @property integer $timestamp
 * @property string $action_params
 *
 * @property User $user0
 */
class LogActions extends ActiveRecord
{
   
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_actions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user', 'context_type', 'action_type', 'timestamp'], 'required'],
            [['user', 'action_type', 'timestamp'], 'integer'],
            [['context_type'], 'string', 'max' => 127],
            [['context'], 'string', 'max' => 255],
            [['user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user' => Yii::t('app', 'User'),
            'context_type' => Yii::t('app', 'Context Type'),
            'context' => Yii::t('app', 'Context'),
            'action_type' => Yii::t('app', 'Action Type'),
            'timestamp' => Yii::t('app', 'Timestamp'),
            'action_params' => Yii::t('app', 'Action Params'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser0()
    {
        return $this->hasOne(User::className(), ['id' => 'user'])->one();
    }
    
    public function renderAttribute($attribute) {
        if($attribute === 'action_type') {
            switch($this->action_type) {
                case static::ACTION_
            }
        }
    }
}
