<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    public $verifyCode;
    
    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
            
            ['verifyCode', 'required', 'on' => ['use-captcha']],
            ['verifyCode', 'captcha', 'on' => ['use-captcha']],
            
            ['username', 'checkAccess'],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        
        if((int)Yii::$app->session->get('login-attemps', 0) > Yii::$app->settings->userLoginAttempsVerification) {
            $this->setScenario('use-captcha');
        }
    }
    
    public function attributeLabels() {
        return [
            'username' => 'Nazwa użytkownika',
            'password' => 'Hasło',
            'rememberMe' => 'Zapamiętaj',
            'verifyCode' => 'Kod weryfikacyjny',
        ];
    }
    
    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Niepoprawna nazwa użytkownika lub hasło.');
            }
        }
    }
    
    public function checkAccess($attribute, $params) {
        if(!$this->hasErrors()) {
            $user = $this->getUser();
            if(!$user || !Yii::$app->authManager->checkAccess($user->id, 'loginPermission')) {
                $this->addError($attribute, 'Nie posiadasz uprawnień do zalogowania się.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if (!$this->validate() || !Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0)) {
            Yii::$app->session->set('login-attemps', (int)Yii::$app->session->get('login-attemps', 0) + 1);
            return false;
        }
        Yii::$app->session->remove('login-attemps');
        return true;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsernameOrEmail($this->username);
        }

        return $this->_user;
    }
}
