<?php

namespace common\models;

use Yii;
use gandh1pl\helpers\ActiveRecord;
use common\widgets\LogActionBehavior;

/**
 * This is the model class for table "settings".
 *
 * @property string $key
 * @property string $value
 */
class Settings extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }
    
    public static $defaultGroup = 'others';
    
    public static function values() {
        return [
            'groups' => [
                'contact' => 'Kontakt',
                'users' => 'Użytkownicy',
                'system' => 'System',
                'others' => 'Inne ustawienia',
            ],
            'keys' => [
                'contactEmail' => [
                    'description' => 'Kontaktowy adres email',
                    'validation' => 'yii\validators\EmailValidator',
                    'group' => 'contact'
                ],
                'facebook-page' => [
                    'description' => "Link do strony na facebook'u",
                    'validation' => 'yii\validators\UrlValidator',
                    'hint' => 'Nie zapomnij o protokole na początku (https://)',
                    'group' => 'contact'
                ],
                // 
                'userLoginAttempsVerification' => [
                    'description' => 'Liczba nieudanych logowań, po których wymagany będzie kod weryfikacyjny.',
                    'defaultValue' => 3,
                    'group' => 'users',
                    'validation' => 'integer'
                ],
                //
                'siteName' => [
                    'description' => 'Nazwa strony',
                    'defaultValue' => 'Yii2lte',
                    'group' => 'system',
                ],
                'metaDescription' => [
                    'description' => 'Opis meta strony (dla wyszukiwarek - krótki opis co się znajduje na stronie)',
                    'group' => 'system'
                ],
                'metaKeywords' => [
                    'description' => 'Słowa kluczowe strony (lista słów, które najlepiej opisują treść strony, oddzielone przecinkami)',
                    'group' => 'system'
                ],
                'googleAnalytics' => [
                    'description' => 'Identyfikator Google Analytics',
                    'hint' => 'Jeśli chcesz używać statystyk Google Analytics, podaj tutaj identyfikator wygenerowany na stronie <a href="https://www.google.com/intl/pl_pl/analytics/" target="_blank">Google Analytics</a>.',
                    'group' => 'system'
                ],
                'cacheDefaultDuration' => [
                    'description' => 'Domyślny czas przechowywania pamięci podręcznej (w sekundach)',
                    'defaultValue' => 86400,
                    'group' => 'system',
                    'validation' => 'integer'
                ],
                'rateLimit' => [
                    'description' => 'Max. ilość żądań HTTP w danym okresie czasu',
                    'defaultValue' => 3,
                    'group' => 'system',
                    'validation' => 'integer'
                ],
                'rateLimitPeriod' => [
                    'description' => 'Okres czasu, w którym będą zliczane żądania HTTP (w sekundach)',
                    'defaultValue' => 1,
                    'group' => 'system',
                    'validation' => 'integer'
                ],
            ],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key'], 'string', 'max' => 50],
            [['value'], 'string', 'max' => 255],
            [['value'], 'validateValue']
        ];
    }
    
    public function validateValue($attr, $params) {
        if(($options = Settings::getValue('keys', $this->key)) === null) {
            return $this->addError('key', 'Nieprawidłowy klucz - wybierz z listy jakie ustawienie chcesz zmienić.');
        }
        if(isset($options['validation'])) {
            $validator = null;
            if(is_string($options['validation'])) {
                $validator = \yii\validators\Validator::createValidator($options['validation'], $this, $attr);
            } elseif(is_array($options['validation'])) {
                $validator = Yii::createObject($options['validation']['class'], $options['validation']);
            }
            $error = null;
            if($validator && !$validator->validate($this->value, $error)) {
                $this->addError('value', $error);
            }
        }
    }
    
    public function getDescription() {
        return static::$keys[$this->key]['description'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'key' => 'Klucz',
            'value' => 'Wartość',
            'description' => 'Opis wartości'
        ];
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        
        Yii::$app->cache->delete('cache_settings');
    }
    
    public function afterDelete() {
        parent::afterDelete();
        
        Yii::$app->cache->delete('cache_settings');
    }
}
