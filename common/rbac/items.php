<?php
return [
    'loginPermission' => [
        'type' => 2,
        'description' => 'Możliwość logowania',
    ],
    'usersManagement' => [
        'type' => 2,
        'description' => 'Zarządzanie użytkownikami',
    ],
    'settingsManagement' => [
        'type' => 2,
        'description' => 'Zarządzanie ustawieniami',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Administrator',
        'children' => [
            'loginPermission',
            'usersManagement',
            'settingsManagement',
        ],
    ],
];
