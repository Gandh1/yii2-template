<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$adminPanelLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
Cześć <?= $user->username ?>,

zostało dla Ciebie utworzone konto na stronie <?= Yii::$app->name ?>.

Aby je aktywować, odwiedź poniższy link i podaj hasło dla swojego nowego konta.

<?= $resetLink ?>

Po aktywowaniu konta można się na nie zalogować pod tym adresem:
<?= $adminPanelLink ?>


----------------------
Jeżeli uważasz, że ten email dotarł do Ciebie przez pomyłkę - przepraszamy i prosimy o zignorowanie go.
