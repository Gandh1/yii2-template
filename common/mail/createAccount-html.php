<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
$adminPanelLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Cześć <?= Html::encode($user->username) ?>,</p>

    <p>zostało dla Ciebie utworzone konto na stronie <?= Html::encode(Yii::$app->name) ?>.</p>
    
    <p>Aby je aktywować, odwiedź poniższy link i podaj hasło dla swojego nowego konta.</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
    
    <p>Po aktywowaniu konta można się na nie zalogować pod tym adresem: <?= Html::a($adminPanelLink, $adminPanelLink) ?></p>
    
    <hr/>
    <p>Jeżeli uważasz, że ten email dotarł do Ciebie przez pomyłkę - przepraszamy i prosimy o zignorowanie go.</p>
</div>
