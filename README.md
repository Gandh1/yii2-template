Yii 2 Advanced Project Template with enhancements
=================================================

*(sorry for bad english :s)*

This template is build on top of [Yii 2 Advanced Project Template](https://github.com/yiisoft/yii2-app-advanced).
Still in development, but it can be used on production.

Some new features / enhancements:
-----------------------------------------
- template is ready to run on shared hosts
    -  frontend: //example.com
    -  backend: //example.com/admin)
- backend is now using AdminLTE layout
- custom widgets and helper classes (from my **still private** repository ;))
- custom version of bootstrap
- styles moved to LESS
- many features ready to use, including
    -  log behavior (saves any change of model to DB / display it etc.)
    -  settings controller (easy changing of some settings, like website name, keywords, facebook fanpage link, etc. + easy adding of new settings)
    -  assigning RBAC roles to users (through backend and console)
    -  email activation of registered users
    -  cache helpers